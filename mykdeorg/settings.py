"""
Local development settings.

Copy to settings.py and edit to suit your needs.
"""

# noinspection PyUnresolvedReferences
from mykdeorg.common_settings import *
import sys
import os
import dj_database_url
import environ

env = environ.Env(
    DEBUG=(bool, False),
    BLENDER_ID_ADDON_CLIENT_ID=(str, 'SPECIAL-SNOWFLAKE-57'),
    DEFAULT_FROM_EMAIL=(str, 'webmaster@localhost'),
    SECRET_KEY=(str, 'my_secret_key'),
    DATABASE_URL=(str, 'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite3'))
)

path = os.path.join(os.path.dirname(__file__), "..", '.env')

# reading .env file
env.read_env(path)

DEBUG = env('DEBUG')
BLENDER_ID_ADDON_CLIENT_ID = env('BLENDER_ID_ADDON_CLIENT_ID')
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')

# Update this to something unique for your machine.
# This was generated using "pwgen -sync 64"
SECRET_KEY = env('SECRET_KEY')

# For testing purposes, allow HTTP as well as HTTPS. Never enable this in production!
# OAUTH2_PROVIDER['ALLOWED_REDIRECT_URI_SCHEMES'] = ['http', 'https']

DATABASES = {
    'default': dj_database_url.parse(env('DATABASE_URL'), conn_max_age=600)
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'
        },
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        # Enable this in production:
        # 'sentry': {
        #     'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
        #     'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        #     # 'tags': {'custom-tag': 'x'},
        # },
    },
    'loggers': {
        'bid_main': {'level': 'DEBUG'},
        'mykdeorg': {'level': 'DEBUG'},
        'bid_api': {'level': 'DEBUG'},
        'bid_addon_support': {'level': 'DEBUG'},
        'sentry.errors': {'level': 'DEBUG', 'handlers': ['console'], 'propagate': False},
    },
    'root': {
        'level': 'WARNING',
        'handlers': [
            'console',
            # Enable this in production:
            # 'sentry',
        ],
    }
}

# For Debug Toolbar, extend with whatever address you use to connect
# to your dev server.
INTERNAL_IPS = ['127.0.0.1']


ALLOWED_HOSTS = ['gallien.kde.org', 'my.kde.org']

# Don't use this in production, but only in tests.
# ALLOWED_HOSTS = ['*']


# # Raven is the Sentry.io integration app for Django. Enable this on production:
# import os
# import raven
# INSTALLED_APPS.append('raven.contrib.django.raven_compat')
#
# RAVEN_CONFIG = {
#     'dsn': 'https://<key>:<secret>@sentry.io/<project>',
#     # If you are using git, you can also automatically configure the
#     # release based on the git info.
#     'release': raven.fetch_git_sha(os.path.abspath(os.curdir)),
# }

# For development, dump email to the console instead of trying to actually send it.
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# Alternatively, run python3 -m smtpd -n -c DebuggingServer -d '0.0.0.0:2525' and set
# EMAIL_PORT = 2525

# Hosts that we allow redirecting to with a next=xxx parameter on the /login and /switch
# endpoints; this is an addition to the defaults, for development purposes.
# NEXT_REDIR_AFTER_LOGIN_ALLOWED_HOSTS.update({
#     'cloud.local:5000', 'cloud.local:5001', 'cloud.local',
# })
